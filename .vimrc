colorscheme gruvbox
set bg=dark
set paste
set pastetoggle=<F2>
set number
set rnu
set mouse=a
set t_Co=256
let g:gruvbox_contrast_dark = 'medium'

syntax on

let g:airline_powerline_fonts = 1
let g:airline_theme='gruvbox'
