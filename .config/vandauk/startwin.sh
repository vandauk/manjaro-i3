#!/bin/bash

echo "Starting Windows 10..."
sudo virsh start win10
sh ~/.dotfiles/1screen.sh
tuned-adm profile virtual-host
sleep 1s
nitrogen --restore
