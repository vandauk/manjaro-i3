#!/bin/bash
# Left + Mid + Right
xrandr --output DVI-D-0 --primary --mode 2560x1440 --pos 1920x0 --rotate normal --output HDMI-0 --mode 1920x1200 --pos 4480x240 --rotate normal --output DP-0 --off --output DP-1 --off --output DP-2 --off --output DP-3 --off --output DP-4 --off --output DP-5 --mode 1920x1200 --pos 0x240 --rotate normal --crtc 0
sleep 1s
nitrogen --restore
