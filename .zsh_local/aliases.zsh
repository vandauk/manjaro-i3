alias  c=clear
alias home='git --work-tree=$HOME --git-dir=$HOME/.home'
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
